const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// [SECTION] MongoDB Connection
// Connect to the database by passing

// connect to db
mongoose
  .connect(
    "mongodb+srv://jroda:admin@zuittbatch243-roda.4axrb9t.mongodb.net/?retryWrites=true&w=majority"
  )
  .then(() => {
    // listen for request
    app.listen(port, () => console.log(`Server running at port ${port}`));
  })
  .catch((err) => {
    console.log(err);
  });

// Set notifications for connection success or failure
// Connection to the database
// Allows to handle errors when the initial connection is establised
// Works with the on and once Mongoose methods

let db = mongoose.connection;

// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));
// [SECTION] Mongoose Schemas
// Schema determine the structure of the documents to be written in the database
// Schema acts as a blueprint
// Use the Schema() constructor of the Mongoose module to create a new Schema Object
const Schema = mongoose.Schema;

const taskSchema = new Schema({
  name: String,
  status: {
    type: String,
    default: "Pending",
  },
});

// [SECTION] Models
// Models use Schemas and they act as the middleman from server (JS code) to our database
// Server > Schema(blueprint) > Database > Collection

const Task = mongoose.model("Task", taskSchema);

// "findOne" is a mongoose method that acts similar to "find" in mongoDB

app.post("/tasks", (req, res) => {
  Task.findOne({ name: req.body.name }, (err, result) => {
    if (result != null && result.name == req.body.name) {
      return res.send("Duplicate Task Found");
    } else {
      let newTask = new Task({
        name: req.body.name,
      });

      newTask.save((err, savedTask) => {
        if (err !== null) {
          return console.log(err.message);
        } else {
          return res.status(201).send("New task created");
        }
      });
    }
  });
});

app.get("/tasks", (req, res) => {
  Task.find({}, (err, result) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});
